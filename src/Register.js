import React, { useState } from "react";
import styles from "./Register.module.css";
import Footer from "./Footer";
import Header from "./Header";
import { addUser } from "./services/usersService";
import { useNavigate } from "react-router-dom";

export default function Register() {
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();

    const handleRegister = async () => {
        if (!username || !email || !password) {
            alert("Please fill out all fields.");
            return;
        }

        const user = {
            username: username,
            email: email,
            password: password,
        };

        try {
            await addUser(user);
            setUsername("");
            setEmail("");
            setPassword("");
            alert("Successfully Registered");
            navigate("/Login");
        } catch (error) {
            console.error("Error while creating an account:", error);
            alert("Failed to register");
        }
    };

    return (
        <div>
            <Header />
            <main className={styles.Body}>
                <div className={styles.Label}>
                    <h3 className={styles.Title}>Register</h3>
                    <label htmlFor="username">Username:</label>
                    <input
                        type="text"
                        id="username"
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                        required
                    />
                </div>
                <div className={styles.Label}>
                    <label htmlFor="email">Email:</label>
                    <input
                        type="email"
                        id="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </div>
                <div className={styles.Label}>
                    <label htmlFor="password">Password:</label>
                    <input
                        type="password"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </div>
                <button className={`${styles.AddTripB} ${styles.animate}`} onClick={handleRegister}>Register</button>
                <div className={styles.RegisterLink}>
                    <p>Already have an account?</p>
                    <a href="./Login">Log In</a>
                </div>
            </main>
            <Footer />
        </div>
    );
}
