import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from 'react-router-dom';
import AuthContext from "./services/AuthContext";
import Footer from "./Footer";
import Header from "./Header";
import styles from "./Home.module.css";
import { getFlights, deleteFlight } from "./services/productService"; 

export default function Home() {
    const { accessToken } = useContext(AuthContext);
    const [flights, setFlights] = useState([]);
    const [filteredFlights, setFilteredFlights] = useState([]);
    const [selectedMonth, setSelectedMonth] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        if (!accessToken) {
            navigate("/login");
        }
    }, [accessToken, navigate]);

    useEffect(() => {
        getFlights()
            .then((response) => {
                setFlights(response);
                setFilteredFlights(response);
            })
            .catch((error) => console.error('Error fetching flights:', error));
    }, []);

    useEffect(() => {
        if (selectedMonth === "") {
            setFilteredFlights(flights);
        } else {
            const filtered = flights.filter(flight => {
                const month = Array.isArray(flight.startTrip) && flight.startTrip.length >= 3 ? flight.startTrip[1] : null;
                return month === parseInt(selectedMonth);
            });
            setFilteredFlights(filtered);
        }
    }, [selectedMonth, flights]);

    async function handleDelete(flightId) {
        try {
            await deleteFlight(flightId);
            const updatedFlights = flights.filter(flight => flight.id !== flightId);
            setFlights(updatedFlights);
            setFilteredFlights(updatedFlights);
            console.log(`Flight with ID ${flightId} deleted successfully`);
        } catch (error) {
            console.error('Error deleting flight:', error);
        }
    }

    const handleClick = (id) => {
        console.log(id);
        navigate(`/${id}/FlightDetail`);
    };

    const handleMonthChange = (event) => {
        setSelectedMonth(event.target.value);
    };

    function renderTrip(t) {
        const startTrip = Array.isArray(t.startTrip) && t.startTrip.length >= 3 ? t.startTrip : [null, null, null];

        return (
            <div className="product" key={t.id}>
                <figure>
                    <figcaption>
                        <a onClick={() => handleClick(t.id)}>
                            <p className={styles.FlightTitle}>{t.title}</p>
                        </a>
                        <div>
                            <span>{startTrip[2] + "-" + startTrip[1] + "-" + startTrip[0]}</span>
                        </div>
                        <p>{t.description}</p>
                        <p>{t.id}</p>
                        <div>
                            <button className={styles.DeleteButton} onClick={() => handleDelete(t.id)}>Delete</button>
                        </div>
                    </figcaption>
                </figure>
            </div>
        );
    }

    return (
        <>
            <div>
                <Header />
                <main>
                    <section id="filters">
                        <label htmlFor="month">Filter by Month:</label>{" "}
                        <select id="month" value={selectedMonth} onChange={handleMonthChange}>
                            <option value="">All months</option>
                            <option value="1">January</option>
                            <option value="2">February</option>
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>
                        </select>
                    </section>
                    <section className={styles.Flights} id="products">
                        {filteredFlights.map(renderTrip)}
                    </section>
                </main>
            </div>
            <Footer />
        </>
    );
}
