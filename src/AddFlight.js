import React, { useState } from "react";
import styles from "./AddFlight.module.css";
import Footer from "./Footer";
import Header from "./Header";
import { addFlight } from "./services/productService"; 
import { redirect } from "react-router-dom";

export default function AddFlight() {
  const [newFlightTitle, setNewFlightTitle] = useState("");
  const [newFlightDescription, setNewFlightDescription] = useState("");
  const [newFlightStartTrip, setNewFlightStartTrip] = useState("");
  const [newFlightEndTrip, setNewFlightEndTrip] = useState("");
  const [meetings, setMeetings] = useState([]);
  const [meetingTitle, setMeetingTitle] = useState("");
  const [meetingDescription, setMeetingDescription] = useState("");

  const handleAddMeeting = () => {
    const newMeeting = {
      id: meetings.length + 1,
      title: meetingTitle,
      description: meetingDescription
    };
    setMeetings([...meetings, newMeeting]);
    setMeetingTitle("");
    setMeetingDescription("");
  };

  const handleAddTrip = async () => {
    const startTripArray = formatDateToArray(newFlightStartTrip);
    const endTripArray = formatDateToArray(newFlightEndTrip);

    const newTrip = {
      title: newFlightTitle,
      description: newFlightDescription,
      startTrip: startTripArray,
      endTrip: endTripArray,
      meetings: meetings
    };

    try {
      await addFlight(newTrip);
      setNewFlightTitle("");
      setNewFlightDescription("");
      setNewFlightStartTrip("");
      setNewFlightEndTrip("");
      setMeetings([]);
      alert("Trip added successfully");
      redirect("/")
    } catch (error) {
      console.error("Error adding trip:", error);
      alert("Failed to add trip");
    }
  };

  const formatDateToArray = (datetime) => {
    const date = new Date(datetime);
    return [
      date.getFullYear(),
      date.getMonth() + 1,
      date.getDate(),
      date.getHours(),
      date.getMinutes()
    ];
  };

  return (
    <div>
      <Header />
      <main className={styles.Body}>
        <div className={styles.Label}>
        <h3 className={styles.Title}>Flight</h3>
          <label htmlFor="newFlightTitle">Title:</label>
          <input
            type="text"
            id="newFlightTitle"
            value={newFlightTitle}
            onChange={(e) => setNewFlightTitle(e.target.value)}
          />
        </div>
        <div className={styles.Label}>
          <label htmlFor="newFlightDescription">Description:</label>
          <input
            type="text"
            id="newFlightDescription"
            value={newFlightDescription}
            onChange={(e) => setNewFlightDescription(e.target.value)}
          />
        </div>
        <div className={styles.Label}>
          <label htmlFor="newFlightStartTrip">Start Trip:</label>
          <input
            type="datetime-local"
            id="newFlightStartTrip"
            value={newFlightStartTrip}
            onChange={(e) => setNewFlightStartTrip(e.target.value)}
          />
        </div>
        <div className={styles.Label}>
          <label htmlFor="newFlightEndTrip">End Trip:</label>
          <input
            type="datetime-local"
            id="newFlightEndTrip"
            value={newFlightEndTrip}
            onChange={(e) => setNewFlightEndTrip(e.target.value)}
          />
        </div>
        <div>
          <h3 className={styles.Title}>Meetings</h3>
          {meetings.map((meeting, index) => (
            <div key={index}>
              <h4>Meeting {index + 1}</h4>
              <p>Title: {meeting.title}</p>
              <p>Description: {meeting.description}</p>
            </div>
          ))}
          <div className={styles.Label}>
            <label htmlFor="meetingTitle">Meeting Title:</label>
            <input
              type="text"
              id="meetingTitle"
              value={meetingTitle}
              onChange={(e) => setMeetingTitle(e.target.value)}
            />
          </div>
          <div className={styles.Label}>
            <label htmlFor="meetingDescription">Meeting Description:</label>
            <input
              type="text"
              id="meetingDescription"
              value={meetingDescription}
              onChange={(e) => setMeetingDescription(e.target.value)}
            />
          </div>
          <button className={styles.AddMeetingB} onClick={handleAddMeeting}>Save - / Add Meeting</button>
          <p className={styles.Information}>Click here to save the meetings</p>
        </div>
        <button className={styles.AddTripB} onClick={handleAddTrip}>Save Trip</button>
      </main>
      <Footer />
    </div>
  );
}
