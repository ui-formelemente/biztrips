import React, { useState, useContext } from "react";
import styles from "./Login.module.css";
import Footer from "./Footer";
import Header from "./Header";
import { useNavigate } from "react-router-dom";
import { authenticateUser } from "./services/usersService";
import AuthContext from "./services/AuthContext";

export default function Login() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const { setAccessToken } = useContext(AuthContext);
    const navigate = useNavigate();

    const handleLogIn = async () => {
        if (!username || !password) {
            alert("Please fill out all fields.");
            return;
        }

        try {
            const token = await authenticateUser(username, password);
            alert("Successfully Logged In");
            setAccessToken(token);
            navigate("/");
        } catch (error) {
            console.error("Error while LogIn:", error);
            alert("Failed to Log In");
        }
    };

    return (
        <div className={styles.container}>
            <Header />
            <main className={styles.main}>
                <div className={styles.formContainer}>
                    <h3 className={styles.title}>Login</h3>
                    <div className={styles.inputBox}>
                        <input
                            type="text"
                            id="username"
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                            required
                        />
                        <span>Username</span>
                    </div>
                    <div className={styles.inputBox}>
                        <input
                            type="password"
                            id="password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                        <span>Password</span>
                    </div>
                    <button className={`${styles.button} ${styles.animate}`} onClick={handleLogIn}>Log In</button>
                    <div className={styles.registerLink}>
                        <p>Do not have an account?</p>
                        <a href="./Register">Register</a>
                    </div>
                </div>
            </main>
            <Footer />
        </div>
    );
}
