import React, { useContext } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import AddFlight from "./AddFlight";
import ChangeFlight from "./ChangeFlight";
import FlightDetail from "./FlightDetail";
import Home from "./Home";
import Login from "./Login";
import Register from "./Register";
import AuthContext from "./services/AuthContext";

export default function App() {
  const { accessToken } = useContext(AuthContext);

  return (
    <BrowserRouter>
      <Routes>
        {accessToken ? (
          <>
            <Route path="/" element={<Home />} />
            <Route path="/AddFlight" element={<AddFlight />} />
            <Route path="/:id/FlightDetail" element={<FlightDetail />} />
            <Route path="/:id/ChangeFlight" element={<ChangeFlight />} />
          </>
        ) : (
          <>
            <Route path="/" element={<Home />} />
            <Route path="/Login" element={<Login />} />
            <Route path="/Register" element={<Register />} />
          </>
        )}
      </Routes>
    </BrowserRouter>
  );
}
