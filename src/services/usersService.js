const baseUrl = process.env.REACT_APP_API_BASE_URL;


//login
export async function authenticateUser({ username, password }) {
  const response = await fetch(`${baseUrl}users?username=${username}&password=${password}`, {
    method: "Post",
    headers: {
      "content-type": "application/json",
      withCredentials: true
    },
    body: JSON.stringify({ username, password })
  })
  if (!response.ok) {
    throw new Error("An error occured while fetching")
  }
  const data = await response.json()
  return data
}


//register
export async function addUser(user) {
  const response = await fetch(baseUrl + "users", {
    method: "POST",
    headers: {
        "content-type": "application/json"
    },
    body: JSON.stringify(user)
})
if (!response.ok) {
    throw new Error("An error occured while fetching")
}
const data = await response.json()
return data
}

// ---------------------------------------------------------------------------------

export async function getAccbyID(user) {
  const response = await fetch(`${baseUrl}/users/${user.id}`)
  if (!response.ok) {
    throw new Error("An error occured while fetching")
  }
  const data = await response.json()
  return data
}


export async function deleteUser(id) {
  const response = await fetch(baseUrl + "users/" + id, {
    method: "DELETE",
  });
  if (response.ok) return response.json();
  throw response;
}

export async function updateUser(id, users) {
  const response = await fetch(baseUrl + "users/" + id, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(users),
  });
  if (response.ok) return response.json();
  throw response;
}
