const baseUrl = process.env.REACT_APP_API_BASE_URL;

export async function getFlights() {
  const response = await fetch(baseUrl + "trips");
  if (response.ok) return response.json();
  throw response;
}

export async function getFlight(id) {
  const response = await fetch(baseUrl + "trips/" + id);
  if (response.ok) return response.json();
  throw response;
}

export async function addFlight(flight) {
  const response = await fetch(baseUrl + "trips", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(flight),
  });
  if (response.ok) return response.json();
  throw response;
}

export async function deleteFlight(id) {
  const response = await fetch(baseUrl + "trips/" + id, {
    method: "DELETE",
  });
  if (response.ok) return response.json();
  throw response;
}

export async function updateFlight(id, flight) {
  const response = await fetch(baseUrl + "trips/" + id, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(flight),
  });
  if (response.ok) return response.json();
  throw response;
}
