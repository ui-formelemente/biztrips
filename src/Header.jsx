import React, { useContext } from "react";
import styles from "./Header.module.css";
import { Link, useNavigate } from "react-router-dom";
import AuthContext from "./services/AuthContext";
export default function Header() {
    const { accessToken, logout } = useContext(AuthContext);
    const navigate = useNavigate();

    const handleLogout = () => {
        logout();
        navigate('/login');
    };

    return (
        <header>
            <nav>
                <ul className={styles.NavList}>
                    <li>
                        <Link to="/">
                            <img width="150px" alt="Business Trips" src="/images/logo.png" />
                        </Link>
                    </li>
                    <div className={styles.Links}>
                        {accessToken ? (
                            <>
                                <li>
                                    <Link to="/AddFlight">Add a Flight</Link>
                                </li>
                                <li>
                                    <button className={styles.LogoutButton} onClick={handleLogout}>Logout</button>
                                </li>
                            </>
                        ) : (
                            <>
                                <li>
                                    <Link to="/Login">Login</Link>
                                </li>
                                <li>
                                    <Link to="/Register">Register</Link>
                                </li>
                            </>
                        )}
                    </div>
                </ul>
            </nav>
        </header>
    );
}
