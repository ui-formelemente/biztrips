import React, { useEffect, useState } from "react";
import { useParams, useNavigate, redirect } from "react-router-dom";
import { getFlight, updateFlight } from "./services/productService";
import styles from "./FlightDetail.module.css";
import Footer from "./Footer";
import Header from "./Header";

export default function FlightDetail() {
    const { id } = useParams();
    const navigate = useNavigate();
    const [flight, setFlight] = useState(null);


    useEffect(() => {
        getFlight(id).then((response) => {
            setFlight(response);
        }).catch((error) => {
            console.error("Error fetching flight:", error);
        });
    }, [id]);

    if (!flight) {
        return <div>Loading...</div>;
    }

    const handleClick = () => {
        navigate(`/${flight.id}/ChangeFlight`);
    };

    return (
        <>
            <Header />
            <div className={styles.Body}>
                <h1>{flight.title}</h1>
                <div className={styles.Information}>
                    <div className={styles.Description}>
                        <p className={styles.DescTitle}>Description:</p>
                        <p className={styles.text}>{flight.description}</p>
                    </div>
                    <div className={styles.Departure}>
                        <p className={styles.DepaTitle}>Start day:</p>
                        <span className={styles.text}>{`${flight.startTrip[2]}-${flight.startTrip[1]}-${flight.startTrip[0]}`}</span>
                    </div>
                    <div className={styles.Departure}>
                        <p className={styles.DepaTitle}>End day:</p>
                        <span className={styles.text}>{`${flight.endTrip[2]}-${flight.endTrip[1]}-${flight.endTrip[0]}`}</span>
                    </div>
                    <div className={styles.Meetings}>
                        <p className={styles.MeetingTitle}>Meetings:</p>
                        {flight.meetings.length > 0 ? (
                            flight.meetings.map((meeting) => (
                                <div key={meeting.id} className={styles.Meeting}>
                                    <p className={styles.MeetingTitle2}>{meeting.title}</p>
                                    <p>{meeting.description}</p>
                                </div>
                            ))
                        ) : (
                            <p>No meetings scheduled.</p>
                        )}
                    </div>
                </div>
                <button onClick={() => { handleClick(flight.id)}}>
                    Edit Flight
                </button>
            </div>
            <Footer />
        </>
    );
};

