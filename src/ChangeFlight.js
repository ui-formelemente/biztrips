import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getFlight, updateFlight } from "./services/productService";
import styles from "./ChangeFlight.module.css";
import Footer from "./Footer";
import Header from "./Header";

export default function ChangeFlight() {
    const { id } = useParams();
    const [flight, setFlight] = useState(null);
    const [editableFlight, setEditableFlight] = useState({
        title: "",
        description: "",
        startTrip: "",
        endTrip: "",
        meetings: []
    });

    useEffect(() => {
        getFlight(id).then((response) => {
            setFlight(response);
            setEditableFlight({
                ...response,
                startTrip: response.startTrip.slice(0, 3).map(num => num.toString().padStart(2, '0')).join('-'),
                endTrip: response.endTrip.slice(0, 3).map(num => num.toString().padStart(2, '0')).join('-')
            });
        }).catch((error) => {
            console.error("Error fetching flight:", error);
        });
    }, [id]);

    if (!flight) {
        return <div>Loading...</div>;
    }

    const handleChange = (e) => {
        const { name, value } = e.target;
        setEditableFlight((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const handleMeetingChange = (index, key, value) => {
        const updatedMeetings = [...editableFlight.meetings];
        updatedMeetings[index][key] = value;
        setEditableFlight((prevState) => ({
            ...prevState,
            meetings: updatedMeetings,
        }));
    };

    const handleDateChange = (name, value) => {
        setEditableFlight((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const handleAddMeeting = () => {
        setEditableFlight((prevState) => ({
            ...prevState,
            meetings: [
                ...prevState.meetings,
                { title: "", description: "" }
            ]
        }));
    };

    const handleSubmit = () => {
        const formattedFlight = {
            ...editableFlight,
            startTrip: editableFlight.startTrip.split('-').map(num => parseInt(num)),
            endTrip: editableFlight.endTrip.split('-').map(num => parseInt(num)),
        };
        updateFlight(id, formattedFlight).then((response) => {
            setFlight(response);
            alert("Flight updated successfully!");
        }).catch((error) => {
            console.error("Error updating flight:", error);
            alert("Failed to update flight.");
        });
    };

    return (
        <>
            <Header />
            <div className={styles.Body}>
                <div className={styles.EditForm}>
                    <h2 className={styles.title}>Edit Flight</h2>
                    <label className={styles.InputText}>
                        <p className={styles.DescTitle}>Title:</p>
                        <input
                            type="text"
                            name="title"
                            value={editableFlight.title}
                            onChange={handleChange}
                        />
                    </label>
                    <label className={styles.InputText}>
                        <p className={styles.DescTitle}>Description:</p>
                        <textarea
                            name="description"
                            value={editableFlight.description}
                            onChange={handleChange}
                        />
                    </label>
                    <label className={styles.InputText}>
                        <p className={styles.DescTitle}>Start day:</p>
                        <input
                            type="date"
                            name="startTrip"
                            value={editableFlight.startTrip}
                            onChange={(e) => handleDateChange("startTrip", e.target.value)}
                        />
                    </label>
                    <label className={styles.InputText}>
                        <p className={styles.DescTitle}>End day:</p>
                        <input
                            type="date"
                            name="endTrip"
                            value={editableFlight.endTrip}
                            onChange={(e) => handleDateChange("endTrip", e.target.value)}
                        />
                    </label>
                    <div className={styles.EditMeeting}>
                        <h3>Meetings</h3>
                        {editableFlight.meetings.map((meeting, index) => (
                            <div key={index} className={styles.MeetingForm}>
                                <label>
                                    Title:
                                    <input
                                        type="text"
                                        value={meeting.title}
                                        onChange={(e) => handleMeetingChange(index, "title", e.target.value)}
                                    />
                                </label>
                                <label>
                                    Description:
                                    <textarea
                                        value={meeting.description}
                                        onChange={(e) => handleMeetingChange(index, "description", e.target.value)}
                                    />
                                </label>
                            </div>
                        ))}
                        <button className={styles.addMeetingButton} type="button" onClick={handleAddMeeting}>
                            Add Meeting
                        </button>
                    </div>
                    <button className={styles.updateButton} type="button" onClick={handleSubmit}>
                        Update Flight
                    </button>
                </div>
            </div>
            <Footer />
        </>
    );
}
